//
//  ContentView.swift
//  PartyLights
//
//  Created by Aaron Trickey on 1/12/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var model = PartyLightsModel()

    var body: some View {
        VStack {
            List(model.bulbs) { bulb in
                HStack {
                    Image(systemName: bulb.isEnabled ? "checkmark.circle.fill" : "circle")
                    Text(bulb.title)
                    Spacer(minLength: 0)
                }
                .contentShape(Rectangle())
                .onTapGesture {
                    model.toggleBulbEnabled(bulb.id)
                }
            }
            switch model.status {
            case .initializing, .waitingForAuthorization:
                Text("Initializing…")
            case .unauthorized:
                Text("No HomeKit permissions")
            case .noHomeAvailable:
                Text("No Homes defined in HomeKit")
            case .ready:
                Button {
                    model.startTheParty()
                } label: {
                    Text("Ready to Party")
                }
            case .partying:
                Button {
                    model.stopTheParty()
                } label: {
                    Text("Partying")
                }
            }
        }
        .onAppear {
            model.engageHomeKit()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
