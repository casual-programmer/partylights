//
//  PartyLightsApp.swift
//  PartyLights
//
//  Created by Aaron Trickey on 1/12/22.
//

import SwiftUI

@main
struct PartyLightsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
