//
//  PartyLightsModel.swift
//  PartyLights
//
//  Created by Aaron Trickey on 1/12/22.
//

import SwiftUI
import HomeKit


struct Bulb: Identifiable, Sendable {
    let id: String
    let title: String
    let service: HMService?
    var isEnabled: Bool

    func getValue<T>(of characteristicType: String) async -> T? {
        guard let c = characteristic(ofType: characteristicType) else {
            return nil
        }
        do {
            try await c.readValue()
            return c.value as? T
        } catch {
            print("failed to read \(characteristicType) from \(title): \(error)")
            return nil
        }
    }

    func setValue(of characteristicType: String, to value: any Sendable) async throws {
        guard let c = characteristic(ofType: characteristicType) else {
            return
        }
        try await c.writeValue(value)
    }

    private func characteristic(ofType type: String) -> HMCharacteristic? {
        service?.characteristics.first { c in c.characteristicType == type }
    }
}


@MainActor
class PartyLightsModel: NSObject, ObservableObject, HMHomeManagerDelegate {
    @Published var status: Status = .initializing
    enum Status {
        case initializing
        case ready
        case partying

        case waitingForAuthorization
        case unauthorized
        case noHomeAvailable
    }

    @Published var bulbs: [Bulb] = []

    /// Call this during startup, or when the view of interest appears; it instantiates the
    /// HMHomeManager, which requests HomeKit permissions if needed, and reloads my state.
    ///
    func engageHomeKit() {
        if isRunningForXcodePreview {
            reload()
            return
        }
        guard manager == nil else {
            return
        }
        manager = HMHomeManager()
        manager!.delegate = self
        reload()
    }

    /// Enable or disable a specific bulb's participation in the party. Initially,
    /// no bulbs are enabled, which means the party is B O R I N G
    ///
    func toggleBulbEnabled(_ id: Bulb.ID) {
        if let i = bulbs.firstIndex(where: { $0.id == id }) {
            bulbs[i].isEnabled.toggle()
        }
    }

    /// Grabs a snapshot of the bulbs's states and starts the timed hue cycle.
    /// No-op unless `status` is `ready`.
    /// Asynchronous; `status` will change when everything's running.
    ///
    func startTheParty() {
        guard status == .ready else {
            return
        }

        status = .partying

        Task {
            print("snapshot had \(snapshot.count) entries")
            await captureSnapshot()

            print("snapshot has \(snapshot.count) entries")

            await writeToAllBulbs([
                HMCharacteristicTypePowerState: true,
                HMCharacteristicTypeSaturation: 75.0,
                HMCharacteristicTypeBrightness: 100.0,
            ])

            timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: { t in
                Task { await self.updateParty() }
            })
        }
    }

    /// Ends the timer and restores the bulbs to their initial condition.
    /// No-op unless `status` is `partying`.
    /// Asynchronous; `status` will change after everything's stopped.
    ///
    func stopTheParty() {
        guard status == .partying else {
            return
        }

        timer?.invalidate()
        timer = nil

        Task {
            await restoreSnapshot()
            status = .ready
        }
    }

    private func writeToAllBulbs(_ vals: [String: any Sendable]) async {
        let bulbs = bulbs

        await withTaskGroup(of: Void.self) { group in
            for bulb in bulbs {
                guard bulb.isEnabled else {
                    continue
                }

                for (key, value) in vals {
                    group.addTask {
                        do {
                            try await bulb.setValue(of: key, to: value)
                        } catch {
                            print(error)
                        }
                    }
                }
            }
        }
    }

    private func captureSnapshot() async {
        let bulbs = bulbs

        snapshot = await withTaskGroup(of: Optional<SnapshotEntry>.self) { @MainActor group in
            for bulb in bulbs {
                guard bulb.isEnabled else {
                    continue
                }

                group.addTask {
                    async let on: Bool? = bulb.getValue(of: HMCharacteristicTypePowerState)
                    async let hue: Double? = bulb.getValue(of: HMCharacteristicTypeHue)
                    async let saturation: Double? = bulb.getValue(of: HMCharacteristicTypeSaturation)
                    async let brightness: Int? = bulb.getValue(of: HMCharacteristicTypeBrightness)

                    return await SnapshotEntry(
                        id: bulb.id,
                        on: on ?? false,
                        hue: hue ?? 0,
                        saturation: saturation ?? 33,
                        brightness: brightness ?? 100
                    )
                }
            }

            var result: [SnapshotEntry] = []
            for await element in group {
                if let element {
                    result.append(element)
                }
            }
            return result
        }
    }

    private func restoreSnapshot() async {
        let snapshot = snapshot

        await withTaskGroup(of: Void.self) { group in
            for entry in snapshot {
                guard let bulb = self.bulbs.first(where: { $0.id == entry.id }) else {
                    continue
                }

                group.addTask {
                    do {
                        try await bulb.setValue(of: HMCharacteristicTypeHue, to: entry.hue)
                        try await bulb.setValue(of: HMCharacteristicTypeSaturation, to: entry.saturation)
                        try await bulb.setValue(of: HMCharacteristicTypeBrightness, to: entry.brightness)
                        try await bulb.setValue(of: HMCharacteristicTypePowerState, to: entry.on)

                    } catch {
                        print(error)
                    }
                }
            }
        }
    }

    private func updateParty() async {
        await writeToAllBulbs([
            HMCharacteristicTypeHue: self.hue
        ])
        hue += 15
        hue.formTruncatingRemainder(dividingBy: 360)
    }

    nonisolated func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {
        Task { await reload() }
    }

    nonisolated func homeManager(_ manager: HMHomeManager, didUpdate status: HMHomeManagerAuthorizationStatus) {
        Task { await reload() }
    }

    private func reload() {
        guard !isRunningForXcodePreview else {
            bulbs = [
                Bulb(id: UUID().uuidString, title: "Left", service: nil, isEnabled: false),
                Bulb(id: UUID().uuidString, title: "Right", service: nil, isEnabled: true),
            ]
            status = .ready
            return
        }

        guard let manager = manager else {
            status = .initializing
            return
        }

        let auth = manager.authorizationStatus

        guard auth.contains(.determined) else {
            status = .waitingForAuthorization
            return
        }

        guard auth.contains(.authorized) else {
            status = .unauthorized
            return
        }

        guard !manager.homes.isEmpty else {
            status = .noHomeAvailable
            return
        }

        var bulbs: [Bulb] = []
        for home in manager.homes {
            for accessory in home.accessories {
                guard accessory.category.categoryType == HMAccessoryCategoryTypeLightbulb else {
                    print("not a bulb: \(accessory.name)")
                    continue
                }

                guard let service = accessory.services.first(where: { $0.serviceType == HMServiceTypeLightbulb }) else {
                    print("doesn't have any lightbulb services: \(accessory.name)")
                    continue
                }

                var bulb = Bulb(id: accessory.uniqueIdentifier.uuidString, title: accessory.name, service: service, isEnabled: false)
                if let old = self.bulbs.first(where: { $0.id == bulb.id }) {
                    bulb.isEnabled = old.isEnabled
                }

                bulbs.append(bulb)
            }
        }
        self.bulbs = bulbs
        status = .ready
    }

    private var manager: HMHomeManager? = nil
    private var timer: Timer? = nil
    private var hue: Double = 1.0

    private var snapshot: [SnapshotEntry] = []
    private struct SnapshotEntry: Identifiable, Sendable {
        var id: Bulb.ID
        var on: Bool
        var hue: Double
        var saturation: Double
        var brightness: Int
    }

    private let isRunningForXcodePreview = {
        ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] == "1"
    }()
}
