
# PartyLights

This is a trivial HomeKit app I built to learn the API. I'm sharing it in case
it helps anyone else with a quick start. I may turn it into something more
later, but for now, it does this:

- Finds all the lightbulbs on your network
- Puts them in a list, so you can select which ones to use
- When you press its one button, which says "Ready to Party", it starts
  cycling the bulbs through hues
- When you press that button again (it now says "Partying"), it stops and
  restores each bulb's initial state

It uses just a minimal set of HomeKit APIs, and runs only in iPhones, and uses
Swift async/await.

More here: <https://casualprogrammer.com/blog/2022/01-20-partylights.html>

